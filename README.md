# Des routes et des ponts

Traduction du livre de Nadia Eghbal, *Roads and Bridges: The Unseen Labor Behind Our Digital Infrastructure* (voir le [PDF](https://ffcontentgrantsviz.blob.core.windows.net/media/2976/roads-and-bridges-the-unseen-labor-behind-our-digital-infrastructure.pdf) de la V.O.).

Ouvrage financé par la Fondation Ford, source sous licence CC BY 4.0, 
